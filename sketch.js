
var pastEvents = [];
var current = new Map();
const PRESS = 144;
const RELEASE = 128;
var keys = [];

const BLACK_WIDTH = 15;
const WHITE_WIDTH = 25;


function setup() {
    createCanvas(1850, 900);
    keyFill();
}

function draw() {
    background(250);
    drawKeyboard();
    let startMillis = new Date().getTime();

    current.forEach(e=>{
        let key = keys[e.key];
        fill(key.fill);
        // let c = color(255, 204, 0);
        // fill(c);
        rect(key.x, 100, key.width, timeScale(startMillis - e.end), key.width/4);
    });

    for (i = 0; i < pastEvents.length; i++) {
        let event = pastEvents[i];
        let key = keys[event.key];

        fill(key.fill);
        // let c = color(255, 204, 0);
        // fill(c);

        rect(key.x , timeScale(startMillis - event.start) + 100, key.width, timeScale(event.start - event.end), key.width/4);
    }

    

    let j = 0;
    for(; j < pastEvents.length; j++){
        if(timeScale(startMillis - pastEvents[j].start) < 2000){
            break;
        }
    }
    if(j >0){
        pastEvents.splice(0, j);
    }


    
}

function drawKeyboard(){
    keys.forEach(k=>{
        if(k.isWhite){
            fill(k.fill);
            rect(k.x, 0, WHITE_WIDTH, 100, 3);
        }
    });

    keys.forEach(k=>{
        if(!k.isWhite){
            fill(k.fill);
            rect(k.x, 0, BLACK_WIDTH, 60, 2);
        }
    });
}

function timeScale(millis){
    return millis / 10 ;
}

function getMIDIMessage(midiMessage) {
    let message = midiMessage.data;
    if(message[0] != 254)
        console.log(message);
    
    if (message[0] === PRESS) {
        
        let event = {key:null, start:null, end:null};
        event.end = new Date().getTime();
        event.key = message[1] - 21;
        current.set(event.key, event);
    } else if (message[0] === RELEASE) {
        let event = current.get(message[1] - 21);
        event.start = new Date().getTime();
        pastEvents.push(event);
        current.delete(event.key);
    }
}

function onMIDISuccess(midiAccess) {
    for (var input of midiAccess.inputs.values())
        input.onmidimessage = getMIDIMessage;
}

function onMIDIFailure() {
    console.log('Nie udało się połączyć do urządzenia midi.');
}

function keyFill(){
    let currentX = 2;
    for(i = 0; i < 88; i++){
        let keyNumb = i % 12;
        let isBlack = keyNumb == 1 || keyNumb == 4 || keyNumb ==6 || keyNumb == 9 || keyNumb == 11;

        if(isBlack){
            keys.push({fill:0, width:15, x : currentX, isWhite: false});
            // currentX += 15;
        }else{
            keys.push({fill:250, width:25, x : currentX, isWhite: true});
            // currentX += 25;
        }

        if(keyNumb == 0 || keyNumb == 5){
            currentX += 18;
        }
        if(keyNumb == 2 || keyNumb == 7){
            currentX += 25;
        }
        if(keyNumb == 3 || keyNumb == 8){
            currentX += 14;
        }
        if(keyNumb == 10){
            currentX += 17;
        }

        if(keyNumb == 1){
            currentX += 5;
        }
        if(keyNumb == 4 || keyNumb == 9){
            currentX += 9;
        }
        if(keyNumb == 6){
            currentX += 5;
        }
        if(keyNumb == 11){
            currentX += 6;
        }


        currentX+=2;
    }



}